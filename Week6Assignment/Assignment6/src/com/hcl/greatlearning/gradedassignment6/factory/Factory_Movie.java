package com.hcl.greatlearning.gradedassignment6.factory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hcl.greatlearning.gradedassignment6.bean.Movie_Coming;
import com.hcl.greatlearning.gradedassignment6.bean.Movies_In_Theaters;
import com.hcl.greatlearning.gradedassignment6.bean.Top_Rated_Indianmovie;
import com.hcl.greatlearning.gradedassignment6.bean.Top_Rated_Movie;
import com.hcl.greatlearning.gradedassignment6.resource.DbResource;

interface MovieOnline  {
	
	public List<String> movieType() throws Exception ;

}
//Factory Pattern
class MovieComing implements MovieOnline{
	//Database Connection
	Connection con=DbResource.getDbConnection();
	public MovieComing() {}
	@Override
	public List<String> movieType() throws Exception {
		
		try {
			List<Movie_Coming> movies=new ArrayList<Movie_Coming>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movie_Comming");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Movie_Coming movie=new Movie_Coming();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setRelease_date(rs.getInt(6));
				movies.add(movie);
			}}catch(Exception e) {
				System.out.println("Movie_Coming"+e);
			}
			
		
		return null;
	}
	
}
class MovieInTheatres implements MovieOnline{
	//Database Connection
	Connection con=DbResource.getDbConnection();
	public MovieInTheatres() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Movies_In_Theaters> movies=new ArrayList<Movies_In_Theaters>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movies_In_Theaters");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Movies_In_Theaters movie=new Movies_In_Theaters();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setRelease_date(rs.getInt(6));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Movies_In_Theaters"+e);
		}
	    return null;	
	}
	
}
class TopRatedIndianMovie implements MovieOnline{
	Connection con=DbResource.getDbConnection();
	public TopRatedIndianMovie() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Top_Rated_Indianmovie> movies=new ArrayList<Top_Rated_Indianmovie>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Indianmovie");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Top_Rated_Indianmovie movie=new Top_Rated_Indianmovie();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenres(rs.getString(3));
				movie.setDuration(rs.getString(4));
				movie.setRelease_date(rs.getInt(5));
				movie.setImdbRating(rs.getInt(6));
				
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Indianmovi"+e);
		}
	    return null;	
	}
	
	
}
class TopRatedMovie implements MovieOnline{
	Connection con=DbResource.getDbConnection();
	public TopRatedMovie() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Top_Rated_Movie> movies=new ArrayList<Top_Rated_Movie>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Movie");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Top_Rated_Movie movie=new Top_Rated_Movie();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenres(rs.getString(3));
				movie.setDuration(rs.getString(4));
				movie.setRelease_date(rs.getInt(5));
				movie.setImdbRating(rs.getInt(6));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Movie"+e);
		}
	    return null;	
	}
}
	
class MovieFactory{
	public static MovieOnline getInstance(String type) {
		if(type.equalsIgnoreCase("MovieComing")) {
			return new MovieComing();
		}else if(type.equalsIgnoreCase("MovieInTheatres")) {
			return new MovieInTheatres();
		}else if(type.equalsIgnoreCase("TopRatedIndianMovie")) {
			return new TopRatedIndianMovie();
		}
		else if(type.equalsIgnoreCase("TopRatedMovie")) {
			return new TopRatedMovie();
		}else {
	    	return null;
	}
}
}

public class Factory_Movie {
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		MovieOnline mo =MovieFactory.getInstance("MovieComingSoon");
		mo.movieType();
	    MovieOnline mo1 =MovieFactory.getInstance("MovieInTheatres");
	    mo1.movieType();
	    MovieOnline mo2 =MovieFactory.getInstance("TopRatedIdianMovie");
	    mo2.movieType();
	    MovieOnline mo3=MovieFactory.getInstance("TopRatedMovie");
	    mo3.movieType();
				
			
		

	}

}
