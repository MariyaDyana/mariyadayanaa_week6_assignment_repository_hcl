package com.hcl.greatlearning.gradedassignment6.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hcl.greatlearning.gradedassignment6.bean.Movie_Coming;

public class Movie_ComingTest {

	//@Test
	public void testMovie_Coming() {
		fail("Not yet implemented");
	}

	//@Test
	public void testMovie_ComingIntStringIntStringStringInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		System.out.println("getId");
		Movie_Coming mc=new Movie_Coming();
		int expResult=1;
		mc.setId(1);
		int result=mc.getId();
		assertEquals(expResult,result);
		
		
		
	}

	@Test
	public void testSetId() {
		//fail("Not yet implemented");
		System.out.println("setId");
		int id=1;
		Movie_Coming mc=new Movie_Coming();
		mc.setId(id);
		assertEquals(mc.getId(),id);
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		System.out.println("getTitle");
		Movie_Coming mc=new Movie_Coming();
		String expResult="Game Night";
		mc.setTitle("Game Night");
		String result=mc.getTitle();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		System.out.println("setTitle");
		String title="Game Night";
		Movie_Coming mc=new Movie_Coming();
		mc.setTitle(title);
		assertEquals(mc.getTitle(),title);
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		System.out.println("getYear");
		Movie_Coming mc=new Movie_Coming();
		int expResult=2018;
		mc.setYear(2018);
		int result=mc.getYear();
		assertEquals(expResult,result);

	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		System.out.println("setYear");
		int year=2018;
		Movie_Coming mc=new Movie_Coming();
		mc.setYear(year);
		assertEquals(mc.getYear(),year);
	}

	@Test
	public void testGetGenres() {
		//fail("Not yet implemented");
		System.out.println("getGenres");
		Movie_Coming mc=new Movie_Coming();
		String expResult="Action";
		mc.setGenres("Action");
		String result=mc.getGenres();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetGenres() {
		//fail("Not yet implemented");
		System.out.println("setGenres");
		String genres="Action";
		Movie_Coming mc=new Movie_Coming();
		mc.setGenres(genres);
		assertEquals(mc.getGenres(),genres);
		}

	@Test
	public void testGetDuration() {
		//fail("Not yet implemented");
		System.out.println("getDuration");
		Movie_Coming mc=new Movie_Coming();
		String expResult="PT100M";
		mc.setDuration("PT100M");
		String result=mc.getDuration();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetDuration() {
		//fail("Not yet implemented");
		System.out.println("setDuration");
		String duration="PT100M";
		Movie_Coming mc=new Movie_Coming();
		mc.setDuration(duration);
		assertEquals(mc.getDuration(),duration);
	}

	@Test
	public void testGetRelease_date() {
		//fail("Not yet implemented");
		System.out.println("getRelease_date");
		Movie_Coming mc=new Movie_Coming();
		int expResult=2018-02-28;
		mc.setRelease_date(2018-02-28);
		int result=mc.getRelease_date();
		assertEquals(expResult,result);

	}

	@Test
	public void testSetRelease_date() {
		//fail("Not yet implemented");
		System.out.println("setRelease_date");
		int release_date=2018-02-28;
		Movie_Coming mc=new Movie_Coming();
		mc.setRelease_date(release_date);
		assertEquals(mc.getRelease_date(),release_date);
	}

	

}
