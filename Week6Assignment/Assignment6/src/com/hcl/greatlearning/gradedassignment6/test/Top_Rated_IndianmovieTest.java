package com.hcl.greatlearning.gradedassignment6.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hcl.greatlearning.gradedassignment6.bean.Top_Rated_Indianmovie;

public class Top_Rated_IndianmovieTest {

	//@Test
	public void testTop_Rated_Indianmovie() {
		fail("Not yet implemented");
	}

	//@Test
	public void testTop_Rated_IndianmovieStringIntStringStringIntInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		System.out.println("getTitle");
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		String expResult="Anand";
		tri.setTitle("Anand");
		String result=tri.getTitle();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		System.out.println("setTitle");
		String title="Anand";
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		tri.setTitle(title);
		assertEquals(tri.getTitle(),title);
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		System.out.println("getYear");
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		int expResult=1971;
		tri.setYear(1971);
		int result=tri.getYear();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		System.out.println("setYear");
		int year=1971;
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		tri.setYear(year);
		assertEquals(tri.getYear(),year);
	}

	@Test
	public void testGetGenres() {
		//fail("Not yet implemented");
		System.out.println("getGenres");
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		String expResult="Drama";
		tri.setGenres("Drama");
		String result=tri.getGenres();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetGenres() {
		//fail("Not yet implemented");
		System.out.println("setGenres");
		String genres="Drama";
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		tri.setGenres(genres);
		assertEquals(tri.getGenres(),genres);
	}

	@Test
	public void testGetDuration() {
		//fail("Not yet implemented");
		System.out.println("getDuration");
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		String expResult="PT122M";
		tri.setDuration("PT122M");
		String result=tri.getDuration();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetDuration() {
		//fail("Not yet implemented");
		System.out.println("setDuration");
		String duration="PT122M";
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		tri.setDuration(duration);
		assertEquals(tri.getDuration(),duration);
	}

	@Test
	public void testGetRelease_date() {
		//fail("Not yet implemented");
		System.out.println("getRelease_date");
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		int expResult=1971-03-12;
		tri.setRelease_date(1971-03-12);
		int result=tri.getRelease_date();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetRelease_date() {
		//fail("Not yet implemented");
		System.out.println("setRelease_date");
		int release_date=1971-03-12;
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		tri.setRelease_date(release_date);
		assertEquals(tri.getRelease_date(),release_date);
	}

	@Test
	public void testGetImdbRating() {
		//fail("Not yet implemented");
		System.out.println("getImdbRating");
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		int expResult=9;
		tri.setImdbRating(9);
		int result=tri.getImdbRating();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetImdbRating() {
		//fail("Not yet implemented");
		System.out.println("setImdbRating");
		int imdbrating=9;
		Top_Rated_Indianmovie tri=new Top_Rated_Indianmovie();
		tri.setImdbRating(imdbrating);
		assertEquals(tri.getImdbRating(),imdbrating);
	}

}
